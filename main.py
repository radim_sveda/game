#Verze s přidáním náhodného pohybu kuličky po startu a změnou barev
import pygame
import random

pygame.init()

# fonty textu
font_20 = pygame.font.SysFont('Verdana', 20)  # změna fontu na Verdana s velikostí 20
font_game_over = pygame.font.SysFont('Verdana', 40, bold=True)  # změna fontu pro "Game Over" na Verdana s velikostí 40 a tučným stylem

# RGB
black = (0, 0, 0)
white = (255, 255, 255)

# obrazovka
width = 900
height = 600

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
width, height = screen.get_width(), screen.get_height()

clock = pygame.time.Clock()
fps = 30

# třída kuličky
class Ball:
    def __init__(self, posx, posy, radius, speed, acceleration, color):
        self.posx = posx
        self.posy = posy
        self.radius = radius
        self.speed = speed
        self.acceleration = acceleration
        self.color = color
        self.x_fac = random.choice([-1, 1]) # náhodný směr pro počáteční x-ovou osu
        self.y_fac = random.choice([-1, 1]) # náhodný směr pro počáteční y-ovou osu
        self.ball = pygame.draw.circle(screen, self.color, (self.posx, self.posy), self.radius)
        self.first_time = 1

    def display(self):
        self.ball = pygame.draw.circle(screen, self.color, (self.posx, self.posy), self.radius)

    def update(self, players):
        self.speed += (1 / 20) * self.acceleration # nastavím jaká akcelerace se bude přičítat k aktuální rychlosti
        self.posx += self.speed * self.x_fac
        self.posy += self.speed * self.y_fac
        if self.posy <= 0 or self.posy >= height:
            self.y_fac *= -1
        elif self.posx <= 0 and self.first_time:
            self.first_time = 0
            return 1
        elif self.posx >= width and self.first_time:
            self.first_time = 0
            return -1
        else:
            # zamezení pohybu kuličky do hráče (aby se to nebugovalo)
            for player in players:
                if player.rect.colliderect(self.get_rect()):
                    if self.x_fac > 0:
                        self.posx = player.rect.left - self.radius
                    else:
                        self.posx = player.rect.right + self.radius
                    self.x_fac *= -1
            return 0

    def reset(self): # resetuje rychlost a pozici kuličky na začátku nové hry
        self.posx = width // 2
        self.posy = height // 2
        self.speed = 7
        self.x_fac = random.choice([-1, 1])  # náhodný směr pro počáteční x-ovou osu
        self.y_fac = random.choice([-1, 1])  # náhodný směr pro počáteční y-ovou osu
        self.first_time = 1

    def hit(self): # změní se směr pohybu kuličky (dopředu / dozadu)
        self.x_fac *= -1

    def get_rect(self): # nastavuje kuličku na čtverec a určujeme jeho velikost
        return pygame.Rect(self.posx - self.radius, self.posy - self.radius, 2 * self.radius, 2 * self.radius)

# třída hráče
class Player:
    def __init__(self, posx, posy, width, height, speed, color):
        self.posx = posx
        self.posy = posy
        self.width = width
        self.height = height
        self.speed = speed
        self.color = color
        self.rect = pygame.Rect(posx, posy, width, height)
        self.surface = pygame.draw.rect(screen, self.color, self.rect)

    def display(self): # vykresluje hráče na obrazocku
        self.surface = pygame.draw.rect(screen, self.color, self.rect)

    def update(self, y_fac): #aktualizuje hráče na ploše a hlídá aby nevyletěl
        self.posy += self.speed * y_fac
        if self.posy <= 0:
            self.posy = 0
        elif self.posy + self.height >= height:
            self.posy = height - self.height
        self.rect = pygame.Rect(self.posx, self.posy, self.width, self.height)

    def display_score(self, text, score, x, y, color): # tobrazování aktuálního skóre
        text = font_20.render(text + str(score), True, color)
        text_rect = text.get_rect()
        text_rect.center = (x, y)
        screen.blit(text, text_rect)

    def get_rect(self):
        return self.rect

def main(): # definice hry
    running = True
    player1 = Player(20, height // 2 - 50, 10, 100, 10, white) # volím si vlastnosti hráče 1
    player2 = Player(width - 30, height // 2 - 50, 10, 100, 10, white) # volím si vlastnosti hráče 2
    ball = Ball(width // 2, height // 2, 7, 7, 0.1, white) # volím si vlastnosti kuličky
    players = [player1, player2]
    player1_score, player2_score = 0, 0
    player1_y_fac, player2_y_fac = 0, 0

    while running:
        screen.fill(black)

        if player1_score >= 1 or player2_score >= 1:
            running = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    player2_y_fac = -1
                if event.key == pygame.K_DOWN:
                    player2_y_fac = 1
                if event.key == pygame.K_w:
                    player1_y_fac = -1
                if event.key == pygame.K_s:
                    player1_y_fac = 1
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    player2_y_fac = 0
                if event.key == pygame.K_w or event.key == pygame.K_s:
                    player1_y_fac = 0
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                restart_rect = pygame.Rect(width // 2 - 50, height // 2 + 100, 100, 40)
                exit_rect = pygame.Rect(width // 2 - 50, height // 2 + 150, 100, 40)
                if restart_rect.collidepoint(mouse_pos):
                    restart_game()  # restart hry
                elif exit_rect.collidepoint(mouse_pos):
                    running = False

        for player in players: # detekce kolize hráče a kuličky
            if pygame.Rect.colliderect(ball.get_rect(), player.get_rect()):
                ball.hit()

        player1.update(player1_y_fac)
        player2.update(player2_y_fac)
        point = ball.update(players)

        if point == -1:
            player1_score += 1
            ball.reset()
        elif point == 1:
            player2_score += 1
            ball.reset()

        player1.display()
        player2.display()
        ball.display()

        player1.display_score("Player 1: ", player1_score, 100, 20, white)
        player2.display_score("Player 2: ", player2_score, width - 100, 20, white)

        pygame.display.update()
        clock.tick(fps)

    show_game_over(player1_score, player2_score)

def restart_game():
    main()

def show_game_over(player1_score, player2_score): # nastavení obrazovky na konci
    screen.fill(black)
    winner_text = font_game_over.render("Game Over", True, white)
    winner_text_rect = winner_text.get_rect(center=(width // 2, height // 2 - 50))
    screen.blit(winner_text, winner_text_rect)

    player1_score_text = font_20.render("Score of Player 1: " + str(player1_score), True, white)
    player1_score_rect = player1_score_text.get_rect(center=(width // 2, height // 2 + 20))
    screen.blit(player1_score_text, player1_score_rect)

    player2_score_text = font_20.render("Score of Player 2: " + str(player2_score), True, white)
    player2_score_rect = player2_score_text.get_rect(center=(width // 2, height // 2 + 60))
    screen.blit(player2_score_text, player2_score_rect)

    restart_text = font_20.render("Restart", True, white) # nastavuju vlastnosti tlačítka Restart na konci hry
    restart_rect = restart_text.get_rect(center=(width // 2, height // 2 + 100))
    pygame.draw.rect(screen, black, restart_rect)  # bez pozadí
    screen.blit(restart_text, restart_rect)

    exit_text = font_20.render("Exit", True, white) # nastavuju vlastnosti tlačítka Exit na konci hry
    exit_rect = exit_text.get_rect(center=(width // 2, height // 2 + 150))
    pygame.draw.rect(screen, black, exit_rect)  # bez pozadí
    screen.blit(exit_text, exit_rect)

    pygame.display.update()

    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                waiting = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos() # získej pozici myši
                if restart_rect.collidepoint(mouse_pos):
                    restart_game()  # restart hry
                elif exit_rect.collidepoint(mouse_pos):
                    pygame.quit() # konec hry
                    return

    pygame.quit()
# spuštění
if __name__ == "__main__":
    main()
